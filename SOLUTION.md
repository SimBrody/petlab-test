# SOLUTION

## Estimation

Estimated: 2.5 hours

Spent: 3 hours

## Solution

Comments on your solution

Node v16 required - v17 causes an error.

In terms of my solution, I am aware it is not finished but I have run out of time. I need to add filters for the remaining elements and add some proper styling.

However, I have some queries about the acceptance criteria:

- it was not clear what information needed to be displayed in the product list table.
- it was not clear how many filters there needed to be
- it was not clear whether filters needed to act together - for example display items which were tagged both dog and cat
- there were 11 items in the database not 12, as specified

In terms of suggestions to improve this product - that depends on whether this is an internal inventory for staff or a product list for customers.
If it were for staff then perhaps information about the current stock numbers for each item or recent sales figures would be useful. I'm assuming they could be accessed via a separate api. Estimate - half a day.

If this were a list for customers,the ability to select or buy products would be useful, so an integration with the app's shopfront. Estimate: 2 days.
