import React, { useEffect, useState } from 'react';
import './App.css';
import { apiData } from './api/apiData';
import ProductCollection from './components/ProductCollection';
import Filters from './components/Filters';

function App() {

  const [data, setData] = useState(null);
  
  const [url, setUrl] = useState('http://localhost:3010/products?_start=0&_end=11');

  useEffect(() => {
    const returnedData = async () => {
      await apiData(url).then((res) => {
        setData(res)
      })
    }
    
    returnedData();
  },[url])
  
  
  return (
    <div className="App ui container grid">
      <h1 className='ui header sixteen wide column'>Pet Lab frontend test</h1>
      <Filters url={url} setUrl={setUrl} initialUrl={url} />
      <ProductCollection products={data}></ProductCollection>
    </div>
  );
}

export default App;
