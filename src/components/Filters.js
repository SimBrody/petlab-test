import { apiData } from "../api/apiData";
import { useState, useEffect } from "react";

const Filters = ({url, setUrl}) => {

    const [data, setData] = useState(null);

    useEffect(() => {
        const returnedData = async () => {
          await apiData(url).then((res) => {
            setData(res)
          })
        }
        
        returnedData();
      },[])

    const setChecked = (e) => {
        let newUrl = url.split('?')[0];
        setUrl(newUrl += `?tags_like=${e.target.value}`);  
    }
    
    const tagArray = [];

    const tagFilters = () => {
        if ( data !== null ) {
            data.forEach(element => {
                element.tags.forEach((tag) => {
                    if (!tagArray.includes(tag)) {
                        tagArray.push(tag)
                    }
                });                
            });
            return tagArray.map((el) => {
                return (
                    <div key={el} className="item">
                        <input type='radio' value={el} name="tag" onChange={setChecked}></input>
                        <label>{el}</label>
                    </div>                    
                )
            })
        } else {
            return 'no tags'
        }
    }

    return (
        <div className="ui container four wide column">
            <h2 className="ui header">Filters</h2>
            <div>{tagFilters()}</div>
        </div>
        
    )
}

export default Filters;