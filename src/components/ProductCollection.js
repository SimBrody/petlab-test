import React, { useEffect } from "react";

const ProductCollection = ({products}) => {
    
    const productList = () => {
        if (products !== null) {
            return products.map((prod) => {
                return (
                    <tr key={prod.id}>
                        <td key={prod.id}>{prod.title}</td>
                    </tr>          
                )
            })
        } else {
            return 'no products'
        }
    }

    useEffect(() => {
        productList();
    },[products]);
    

    return (
        <div className="ui eight wide column">
            <table className="ui celled table">
                <thead><tr><th>Product list</th></tr></thead>
                <tbody>{productList()}</tbody>
            </table>
        </div>
    )
}

export default ProductCollection;