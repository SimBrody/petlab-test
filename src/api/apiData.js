
export const apiData = (url) => {
    
    const data = async () => {
        const response = await fetch(url)
            .then((res) => res.json())
            .catch(() => null);
        return response;
    }
    
    return data();
    
}
